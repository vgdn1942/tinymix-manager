import 'package:flutter/material.dart';
import 'package:tinymix_manager/localization/app_localizations.dart';
import 'package:tinymix_manager/shared_preferences_helper.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);
    final showControlNumber =
        SharedPreferencesHelper.instance.getShowControlNumber();
    return Scaffold(
      appBar: AppBar(
        title: Text(appLocalizations.settings),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(appLocalizations.showControlNumber),
            subtitle: Text(
              showControlNumber
                  ? appLocalizations.scrollbarLabelOff
                  : appLocalizations.scrollbarLabelOn,
            ),
            trailing: Switch(
              value: showControlNumber,
              onChanged: (value) {
                setState(() {
                  SharedPreferencesHelper.instance.setShowControlNumber(value);
                });
              },
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}

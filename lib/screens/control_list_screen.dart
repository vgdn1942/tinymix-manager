import 'dart:async';
import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tinymix_manager/database/database_helper.dart';
import 'package:tinymix_manager/database/models/history_control.dart';
import 'package:tinymix_manager/localization/app_localizations.dart';
import 'package:tinymix_manager/models/control.dart';
import 'package:tinymix_manager/models/tinymix_output.dart';
import 'package:tinymix_manager/screens/settings_screen.dart';
import 'package:tinymix_manager/shared_preferences_helper.dart';
import 'package:tinymix_manager/widgets/control_details_dialog.dart';
import 'package:share/share.dart';
import 'package:tinymix_manager/widgets/control_list_placeholder.dart';
import 'package:tinymix_manager/widgets/draggable_scrollbar_rrect.dart';
import 'package:tinymix_manager/widgets/history_control_list_placeholder.dart';
import 'package:tinymix_manager/widgets/icon_info.dart';

//main screen
class ControlListScreen extends StatefulWidget {
  //height of list tiles when filter bar is not present
  //can be not calculated for performance reasons
  static const double defaultControlListItemExtent = 72;
  static const double defaultHistoryItemExtent = 85;

  @override
  _ControlListScreenState createState() => _ControlListScreenState();
}

class _ControlListScreenState extends State<ControlListScreen>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  static const channel = const MethodChannel('tinymix');

  final scaffoldKey = GlobalKey<ScaffoldState>();

  AppLocalizations appLocalizations;

  bool isLoading = false;

  bool showControlNumber;

  //----------Controllers---------------

  ScrollController controlListScrollController = ScrollController();
  ScrollController favoritesListScrollController = ScrollController();
  ScrollController historyListScrollController = ScrollController();

  final searchController = TextEditingController();

  //for emphasize user control that he tapped on history list
  AnimationController blinkAnimationController;

  //-----------Controllers End----------

  //determines ListView index to blink, when -1 no items blinks
  int blinkControlIndex = -1;

  int bottomNavigationBarIndex =
      SharedPreferencesHelper.instance.getBottomNavigationBarIndex();

  //filter bar not present on startup
  bool isAppBarBottomVisible = false;

  //choosed types of controls to filter
  List<ControlType> filterOptions = List.of(ControlType.values);

  //---------Futures------------

  //is app has superuser privileges
  bool isSuOn;
  Future<bool> isSuOnFuture;

  TinymixOutput tinymixOutput;
  Future<TinymixOutput> tinymixOutputFuture;

  List<int> favoritesCtl;
  Future<List<int>> favoritesCtlFuture;

  Map<int, String> controlsInitialValue;
  Future<Map<int, String>> controlsInitialValueFuture;

  List<HistoryControl> historyControls;
  Future<List<HistoryControl>> historyControlsFuture;

  //----------Futures End---------

  //reload futures when needs updated values

  void reloadTinymixOutput() {
    tinymixOutputFuture = getTinymixOutputFuture();
  }

  Future<TinymixOutput> getTinymixOutputFuture() async {
    final tinymixOutputRaw =
        await channel.invokeListMethod<String>('getTinymixOutput');
    return TinymixOutput(tinymixOutputRaw);
  }

  void reloadAllFavorites() {
    favoritesCtlFuture = DatabaseHelper.instance.getAllFavorites();
  }

  void reloadAllControlInitialValue() {
    controlsInitialValueFuture =
        DatabaseHelper.instance.getAllControlInitialValue();
  }

  void reloadAllHistoryControl() {
    historyControlsFuture = DatabaseHelper.instance.getAllHistoryControl();
  }

  @override
  void initState() {
    super.initState();

    blinkAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
      upperBound: 0.3,
    );

    //for saving choosed NavigationBar index
    WidgetsBinding.instance.addObserver(this);

    //only one time fetch
    isSuOnFuture = channel.invokeMethod('getSu');

    reloadTinymixOutput();
    reloadAllFavorites();
    reloadAllControlInitialValue();
    reloadAllHistoryControl();
  }

  //when app is about to close save choosed NavigationBar index
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);

    if (state == AppLifecycleState.paused) {
      SharedPreferencesHelper.instance
          .setBottomNavigationBarIndex(bottomNavigationBarIndex);
    }
  }

  @override
  void dispose() {
    blinkAnimationController.dispose();

    super.dispose();

    controlListScrollController.dispose();
    favoritesListScrollController.dispose();
    historyListScrollController.dispose();

    //for saving choosed NavigationBar index
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    showControlNumber = SharedPreferencesHelper.instance.getShowControlNumber();

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Tinymix Manager'),
        actions: buildAppBarActions(context),
        bottom: isAppBarBottomVisible ? buildAppBarBottom() : null,
      ),
      body: buildBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: bottomNavigationBarIndex,
        onTap: (index) {
          setState(() {
            if (bottomNavigationBarIndex != index) {
              isAppBarBottomVisible = false;
              bottomNavigationBarIndex = index;
            }
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text(appLocalizations.all),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text(appLocalizations.favorites),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            title: Text(appLocalizations.history),
          ),
        ],
      ),
    );
  }

  Widget buildAppBarBottom() {
    return PreferredSize(
      preferredSize: Size.fromHeight(102),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
        child: Column(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(21),
                  borderSide: BorderSide.none,
                ),
                fillColor: Colors.white,
                filled: true,
                contentPadding: EdgeInsets.symmetric(horizontal: 16),
                hintText: appLocalizations.typeControlName,
              ),
              onChanged: (text) {
                setState(() {});
              },
              controller: searchController,
            ),
            Divider(
              height: 6,
              color: Colors.transparent,
            ),
            SizedBox.fromSize(
              size: Size.fromHeight(40),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: ControlType.values.map((option) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: FilterChip(
                      selected: filterOptions.contains(option),
                      label: Text(option.name),
                      onSelected: (isSelected) {
                        setState(() {
                          if (isSelected) {
                            filterOptions.add(option);
                          } else {
                            filterOptions.remove(option);
                          }
                        });
                      },
                    ),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> buildAppBarActions(BuildContext context) {
    List actions = <Widget>[
      IconButton(
        icon: Icon(Icons.filter_list),
        onPressed: onPressedFilterButton,
      ),
    ];

    if (bottomNavigationBarIndex != 1) {
      actions.add(buildPopupMenuButton(context));
    }

    return actions;
  }

  void onPressedFilterButton() {
    setState(() {
      if (isAppBarBottomVisible) {
        searchController.text = '';
        filterOptions = List.of(ControlType.values);
      } else {
        jumpToTop();
      }

      isAppBarBottomVisible = !isAppBarBottomVisible;
    });
  }

  void jumpToTop() {
    if (controlListScrollController.hasClients) {
      controlListScrollController.jumpTo(0);
    } else if (favoritesListScrollController.hasClients) {
      favoritesListScrollController.jumpTo(0);
    } else if (historyListScrollController.hasClients) {
      historyListScrollController.jumpTo(0);
    }
  }

  void undoAllChanges() async {
    final output = await showUndoDialog(context);

    if (output == null || !output) {
      return;
    }

    setState(() {
      isLoading = true;
    });

    for (final initialValueCtl in controlsInitialValue.keys) {
      final control = tinymixOutput.controls[initialValueCtl];
      final initialValue = controlsInitialValue[initialValueCtl];

      await channel.invokeMethod(
        'setControl',
        [
          control.name,
          initialValue,
        ],
      );

      await insertHistoryControl(
        control: control,
        oldValue: control.valueAsCommandArgument,
        newValue: initialValue,
      );
    }

    await DatabaseHelper.instance.deleteAllControlInitialValue();

    setState(() {
      reloadTinymixOutput();
      reloadAllControlInitialValue();
      reloadAllHistoryControl();

      isLoading = false;
    });

    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(appLocalizations.allChangesSuccessfullyReverted),
      ),
    );
  }

  void clearHistory() async {
    setState(() {
      isLoading = true;
    });

    await DatabaseHelper.instance.deleteAllHistoryControl();

    setState(() {
      reloadAllHistoryControl();

      isLoading = false;
    });
  }

  PopupMenuButton<PopupMenuOption> buildPopupMenuButton(BuildContext context) {
    return PopupMenuButton<PopupMenuOption>(
      onSelected: (option) {
        switch (option) {
          case PopupMenuOption.info:
            if (tinymixOutput == null) {
              return;
            }

            showInfoDialog(context);
            break;
          case PopupMenuOption.undoAllChanges:
            undoAllChanges();
            break;
          case PopupMenuOption.clearHistory:
            clearHistory();
            break;
          case PopupMenuOption.settings:
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingsScreen(),
              ),
            );
            break;
          default:
            throw Exception();
        }
      },
      itemBuilder: (context) {
        List<PopupMenuItem<PopupMenuOption>> items = [];

        if (bottomNavigationBarIndex == 0) {
          items.add(
            PopupMenuItem<PopupMenuOption>(
              value: PopupMenuOption.info,
              child: Text(appLocalizations.info),
            ),
          );
          items.add(
            PopupMenuItem<PopupMenuOption>(
              value: PopupMenuOption.settings,
              child: Text(appLocalizations.settings),
            ),
          );
          items.add(
            PopupMenuItem<PopupMenuOption>(
              value: PopupMenuOption.undoAllChanges,
              enabled: controlsInitialValue?.isNotEmpty ?? false,
              child: Text(
                appLocalizations.undoAllChanges,
                style: TextStyle(
                  color: controlsInitialValue?.isNotEmpty ?? false
                      ? Colors.red
                      : Colors.grey,
                ),
              ),
            ),
          );
        }

        if (bottomNavigationBarIndex == 2) {
          items.add(
            PopupMenuItem<PopupMenuOption>(
              value: PopupMenuOption.clearHistory,
              enabled: historyControls?.isNotEmpty ?? false,
              child: Text(
                appLocalizations.clearHistory,
                style: TextStyle(
                  color: historyControls?.isNotEmpty ?? false
                      ? Colors.red
                      : Colors.grey,
                ),
              ),
            ),
          );
        }

        return items;
      },
    );
  }

  Future<bool> showUndoDialog(BuildContext context) {
    return showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(
            appLocalizations.sureUndoAllControls,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(appLocalizations.cancelCapital),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
            FlatButton(
              child: Text(
                appLocalizations.undoCapital,
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                Navigator.pop(context, true);
              },
            ),
          ],
        );
      },
    );
  }

  Future showInfoDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                title: Text('${appLocalizations.mixerName}: '),
                subtitle: Text(tinymixOutput.mixerName),
              ),
              ListTile(
                title: Text('${appLocalizations.numberOfControls}: '),
                subtitle: Text(tinymixOutput.controls.length.toString()),
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(appLocalizations.okCapital),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Widget buildBody() {
    Widget body;
    if (bottomNavigationBarIndex == 2) {
      body = buildHistoryBody();
    } else {
      body = buildControlListBody();
    }

    if (isLoading) {
      body = Stack(
        children: <Widget>[
          LinearProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.pink),
            backgroundColor: Colors.pink[200],
          ),
          body,
        ],
      );
    }

    return body;
  }

  String getControlTitle(String controlName, int controlCtl) {
    if (showControlNumber) {
      return '$controlCtl $controlName';
    }

    return controlName;
  }

  Widget buildControlListBody() {
    return FutureBuilder<bool>(
      initialData: isSuOn,
      future: isSuOnFuture,
      builder: (context, isSuOnSnapshot) {
        return FutureBuilder<TinymixOutput>(
          initialData: tinymixOutput,
          future: tinymixOutputFuture,
          builder: (context, tinymixOutputSnapshot) {
            return FutureBuilder<List<int>>(
              initialData: favoritesCtl,
              future: favoritesCtlFuture,
              builder: (context, favoritesCtlSnapshot) {
                return FutureBuilder<Map<int, String>>(
                  initialData: controlsInitialValue,
                  future: controlsInitialValueFuture,
                  builder: (context, controlsInitialValueSnapshot) {
                    if (isSuOnSnapshot.data == null ||
                        tinymixOutputSnapshot.data == null ||
                        favoritesCtlSnapshot.data == null ||
                        controlsInitialValueSnapshot.data == null) {
                      return const ControlListPlaceholder();
                    }

                    isSuOn = isSuOnSnapshot.data;
                    tinymixOutput = tinymixOutputSnapshot.data;
                    favoritesCtl = favoritesCtlSnapshot.data;
                    controlsInitialValue = controlsInitialValueSnapshot.data;

                    //not actually working
                    if (!isSuOn) {
                      return IconInfo(
                        info: appLocalizations.noRoot,
                        iconData: Icons.perm_device_information,
                      );
                    }

                    final controls = tinymixOutput.controls;

                    final isFavoritesBar = bottomNavigationBarIndex == 1;

                    if (isFavoritesBar && favoritesCtl.isEmpty) {
                      return IconInfo(
                        info: appLocalizations.noFavorites,
                        iconData: Icons.favorite_border,
                      );
                    }

                    List<Control> favoriteControls;
                    if (isFavoritesBar) {
                      favoriteControls =
                          favoritesCtl.map((ctl) => controls[ctl]).toList();
                    } else if (blinkControlIndex > -1) {
                      WidgetsBinding.instance.addPostFrameCallback((duration) {
                        blinkControl();
                      });
                    }

                    return DraggableScrollbarRrect(
                      controller: isFavoritesBar
                          ? favoritesListScrollController
                          : controlListScrollController,
                      itemExtent:
                          ControlListScreen.defaultControlListItemExtent,
                      showLabel: !showControlNumber && !isFavoritesBar,
                      child: ListView.builder(
                        key: PageStorageKey(
                          isFavoritesBar ? 'favoriteList' : 'controlList',
                        ),
                        itemExtent: isAppBarBottomVisible
                            ? null
                            : ControlListScreen.defaultControlListItemExtent,
                        controller: isFavoritesBar
                            ? favoritesListScrollController
                            : controlListScrollController,
                        itemCount: isFavoritesBar
                            ? favoriteControls.length
                            : controls.length,
                        itemBuilder: (context, index) {
                          final control = isFavoritesBar
                              ? favoriteControls[index]
                              : controls[index];
                          final isFavorite = isFavoritesBar ||
                              favoritesCtl.contains(control.ctl);
                          final isModified =
                              controlsInitialValue.containsKey(control.ctl);

                          if (!isAppBarBottomVisible ||
                              isAppBarBottomVisible &&
                                  control.name.toLowerCase().contains(
                                      searchController.text.toLowerCase()) &&
                                  filterOptions.contains(control.type)) {
                            Widget title = Text(
                              getControlTitle(control.name, control.ctl),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            );

                            if (isModified) {
                              title = Row(
                                children: [
                                  Container(
                                    width: 6,
                                    height: 6,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(3),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  const Divider(indent: 4),
                                  Flexible(
                                    child: title,
                                  ),
                                ],
                              );
                            }

                            Widget tile = ListTile(
                              title: title,
                              subtitle: Text(
                                control.valueAsString,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              trailing: !isFavorite || isFavoritesBar
                                  ? null
                                  : Icon(Icons.favorite),
                              onTap: () {
                                showChangeControlDialog(control);
                              },
                              onLongPress: () {
                                showControlBottomSheet(control, isFavorite);
                              },
                            );

                            if (blinkControlIndex == index) {
                              tile = Stack(
                                children: <Widget>[
                                  FadeTransition(
                                    opacity: blinkAnimationController,
                                    child: Container(
                                      color: Theme.of(context).primaryColor,
                                      width: double.maxFinite,
                                      height: ControlListScreen
                                          .defaultControlListItemExtent,
                                    ),
                                  ),
                                  tile,
                                ],
                              );
                            }

                            return Column(
                              children: <Widget>[
                                tile,
                                const Divider(height: 0),
                              ],
                            );
                          }

                          return Container(height: 0.0000000001);
                        },
                      ),
                    );
                  },
                );
              },
            );
          },
        );
      },
    );
  }

  Widget buildHistoryBody() {
    return FutureBuilder<List<HistoryControl>>(
      initialData: historyControls,
      future: historyControlsFuture,
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return const HistoryControlListPlaceholder();
        }

        historyControls = snapshot.data;

        if (historyControls.isEmpty) {
          return IconInfo(
            info: appLocalizations.noHistory,
            iconData: Icons.history,
          );
        }

        return DraggableScrollbarRrect(
          controller: historyListScrollController,
          itemExtent: ControlListScreen.defaultHistoryItemExtent,
          showLabel: false,
          child: ListView.builder(
            key: PageStorageKey('historyList'),
            controller: historyListScrollController,
            itemExtent: isAppBarBottomVisible
                ? null
                : ControlListScreen.defaultHistoryItemExtent,
            itemBuilder: (context, index) {
              final historyControl = historyControls[index];

              if (!isAppBarBottomVisible ||
                  isAppBarBottomVisible &&
                      historyControl.name
                          .toLowerCase()
                          .contains(searchController.text.toLowerCase()) &&
                      filterOptions.contains(historyControl.type)) {
                return Column(
                  children: <Widget>[
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 13,
                        horizontal: 16,
                      ),
                      title: Text(
                        getControlTitle(
                          historyControl.name,
                          historyControl.ctl,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '${historyControl.oldValueAsString}'
                            ' -> '
                            '${historyControl.newValueAsString}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(historyControl.dateReadable),
                        ],
                      ),
                      onTap: () {
                        seeControl(historyControl);
                      },
                      onLongPress: () {
                        showHistoryBottomSheet(historyControl);
                      },
                    ),
                    Divider(height: 0),
                  ],
                );
              }

              return Container(height: 0.0000000001);
            },
            itemCount: historyControls.length,
          ),
        );
      },
    );
  }

  double getControlScrollOffset(int ctl) {
    final appBarHeight = scaffoldKey.currentState.appBarMaxHeight;
    final halfScreenHeight = MediaQuery.of(context).size.height / 2;
    final halfControlItemHeight =
        ControlListScreen.defaultControlListItemExtent / 2;

    return math.max(
      0.0,
      ControlListScreen.defaultControlListItemExtent * ctl -
          halfScreenHeight +
          appBarHeight +
          halfControlItemHeight,
    );
  }

  void blinkControl() async {
    await controlListScrollController.animateTo(
      getControlScrollOffset(blinkControlIndex),
      duration: Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
    );

    await blinkAnimationController.forward();
    await blinkAnimationController.reverse();
    await blinkAnimationController.forward();
    await blinkAnimationController.reverse();

    setState(() {
      blinkControlIndex = -1;
    });
  }

  void seeControl(HistoryControl historyControl) {
    setState(() {
      isAppBarBottomVisible = false;
      bottomNavigationBarIndex = 0;
      blinkControlIndex = historyControl.ctl;
    });
  }

  void showHistoryBottomSheet(HistoryControl historyControl) async {
    final isWantDelete = await showModalBottomSheet<bool>(
      context: context,
      builder: (context) {
        return ListTile(
          title: Text(appLocalizations.deleteBottomSheetAction),
          leading: Icon(Icons.delete),
          onTap: () {
            Navigator.pop(
              context,
              true,
            );
          },
        );
      },
    );

    if (isWantDelete == null || !isWantDelete) {
      return;
    }

    setState(() {
      isLoading = true;
    });

    await DatabaseHelper.instance.deleteHistoryControl(historyControl);

    setState(() {
      reloadAllHistoryControl();

      isLoading = false;
    });
  }

  void showControlBottomSheet(Control control, bool isFavorite) {
    showModalBottomSheet<ModalBottomSheetOption>(
      context: context,
      builder: (context) {
        List<Widget> actions = [
          ListTile(
            title: Text(isFavorite
                ? appLocalizations.removeFromFavorites
                : appLocalizations.addToFavorites),
            leading: Icon(isFavorite ? Icons.favorite_border : Icons.favorite),
            onTap: () {
              Navigator.pop(
                context,
                ModalBottomSheetOption.favorites,
              );
            },
          ),
          ListTile(
            title: Text(appLocalizations.shareTerminalCommand),
            leading: Icon(Icons.share),
            onTap: () {
              Navigator.pop(
                context,
                ModalBottomSheetOption.share,
              );
            },
          ),
        ];

        if (controlsInitialValue.containsKey(control.ctl)) {
          actions.add(
            ListTile(
              title: Text(appLocalizations.undoChanges),
              leading: Icon(Icons.undo),
              onTap: () {
                Navigator.pop(
                  context,
                  ModalBottomSheetOption.undoChanges,
                );
              },
            ),
          );
        }

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: actions,
        );
      },
    ).then((controlAction) {
      if (controlAction != null) {
        switch (controlAction) {
          case ModalBottomSheetOption.favorites:
            changeControlFavorite(control, isFavorite);
            break;
          case ModalBottomSheetOption.share:
            Share.share('tinymix \"${control.name}\"');
            break;
          case ModalBottomSheetOption.undoChanges:
            undoChanges(control);
            break;
          default:
            throw Exception();
        }
      }
    });
  }

  void undoChanges(Control control) async {
    setState(() {
      isLoading = true;
    });

    await channel.invokeMethod(
      'setControl',
      [
        control.name,
        controlsInitialValue[control.ctl],
      ],
    );

    await insertHistoryControl(
      control: control,
      oldValue: control.valueAsCommandArgument,
      newValue: controlsInitialValue[control.ctl],
    );

    await DatabaseHelper.instance.deleteControlInitialValue(control.ctl);

    setState(() {
      reloadTinymixOutput();
      reloadAllControlInitialValue();
      reloadAllHistoryControl();

      isLoading = false;
    });

    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text('${control.name}: ${appLocalizations.changesReverted}'),
      ),
    );
  }

  void changeControlFavorite(Control control, bool isFavorite) async {
    setState(() {
      isLoading = true;
    });

    String snackBarText;
    if (isFavorite) {
      snackBarText = appLocalizations.removedFromFavorites;
      await DatabaseHelper.instance.deleteFavorite(control.ctl);
    } else {
      snackBarText = appLocalizations.addedToFavorites;
      await DatabaseHelper.instance.insertFavorite(control.ctl);
    }

    setState(() {
      reloadAllFavorites();

      isLoading = false;
    });

    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text('${control.name}: $snackBarText'),
      ),
    );
  }

  void changeControl(
    Control control,
    String oldValue,
    String newValue,
  ) async {
    setState(() {
      isLoading = true;
    });

    await channel.invokeMethod(
      'setControl',
      [control.name, newValue],
    );

    var upToDateValue = await channel.invokeMethod(
      'getControlValue',
      control.name,
    );

    if (control.type == ControlType.BOOL) {
      upToDateValue = upToDateValue == 'On' ? '1' : '0';
    }

    final isControlChanged = upToDateValue == newValue;

    if (isControlChanged) {
      await insertHistoryControl(
        control: control,
        oldValue: oldValue,
        newValue: newValue,
      );

      bool isControlInitialValueChanged = true;
      if (!controlsInitialValue.containsKey(control.ctl)) {
        await DatabaseHelper.instance
            .insertControlInitialValue(control.ctl, oldValue);
      } else if (newValue == controlsInitialValue[control.ctl]) {
        await DatabaseHelper.instance.deleteControlInitialValue(control.ctl);
      } else {
        isControlInitialValueChanged = false;
      }

      setState(() {
        if (isControlInitialValueChanged) {
          reloadAllControlInitialValue();
        }

        reloadTinymixOutput();
        reloadAllHistoryControl();

        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }

    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: isControlChanged
            ? Text('${control.name}: ${appLocalizations.changed}')
            : Text('${control.name}: ${appLocalizations.notChanged}'),
        action: isControlChanged
            ? SnackBarAction(
                label: appLocalizations.undoCapital,
                onPressed: () {
                  undoChangeControl(
                    control,
                    oldValue,
                    newValue,
                  );
                },
              )
            : null,
      ),
    );
  }

  void showChangeControlDialog(Control control) {
    showDialog<String>(
      context: context,
      builder: (context) {
        return buildChangeControlDialog(context, control);
      },
    ).then((newValue) {
      final oldValue = control.valueAsCommandArgument;

      if (newValue != null && newValue != oldValue) {
        changeControl(control, oldValue, newValue);
      }
    });
  }

  void undoChangeControl(
    Control control,
    String oldValue,
    String newValue,
  ) async {
    setState(() {
      isLoading = true;
    });

    await channel.invokeMethod(
      'setControl',
      [
        control.name,
        oldValue,
      ],
    );

    await insertHistoryControl(
      control: control,
      oldValue: newValue,
      newValue: oldValue,
    );

    bool isControlInitialValueChanged =
        oldValue == controlsInitialValue[control.ctl];
    if (isControlInitialValueChanged) {
      await DatabaseHelper.instance.deleteControlInitialValue(control.ctl);
    }

    setState(() {
      if (isControlInitialValueChanged) {
        reloadAllControlInitialValue();
      }

      reloadTinymixOutput();
      reloadAllHistoryControl();

      isLoading = false;
    });
  }

  Future<void> insertHistoryControl({
    @required Control control,
    @required String oldValue,
    @required String newValue,
  }) async {
    final date = DateTime.now().toIso8601String();

    await DatabaseHelper.instance.insertHistoryControl(
      HistoryControl(
        date: date,
        ctl: control.ctl,
        name: control.name,
        type: control.type,
        oldValue: oldValue,
        newValue: newValue,
      ),
    );
  }

  Widget buildChangeControlDialog(BuildContext context, Control control) {
    return FutureBuilder<String>(
      future: channel.invokeMethod('getControl', control.name),
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return ControlDetailsDialog(control, snapshot.data);
      },
    );
  }
}

enum PopupMenuOption { info, undoAllChanges, clearHistory, settings }

enum ModalBottomSheetOption { favorites, share, undoChanges }

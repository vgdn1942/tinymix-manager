import 'package:flutter/material.dart';
import 'package:tinymix_manager/localization/app_localizations.dart';
import 'package:tinymix_manager/models/control.dart';

class ControlDetailsDialog extends StatefulWidget {
  final Control control;
  final String details;

  const ControlDetailsDialog(this.control, this.details, {Key key})
      : super(key: key);

  @override
  _ControlDetailsDialogState createState() => _ControlDetailsDialogState();
}

class _ControlDetailsDialogState extends State<ControlDetailsDialog> {
  final okButtonKey = GlobalKey();

  var okButtonEnabled = true;

  List<TextEditingController> textControllersINT;
  List<String> selectedENUM;
  bool switchBOOL;

  @override
  Widget build(BuildContext context) {
    Widget title;
    Widget content;
    Function onOkButtonPressed;

    switch (widget.control.type) {
      case ControlType.INT:
        title = buildINTTitle();
        content = buildINTContent();
        onOkButtonPressed = onOkButtonPressedINT;
        break;
      case ControlType.ENUM:
        title = buildENUMTitle();
        content = buildENUMContent();
        onOkButtonPressed = onOkButtonPressedENUM;
        break;
      case ControlType.BOOL:
        title = buildBOOLTitle();
        content = buildBOOLContent();
        onOkButtonPressed = onOkButtonPressedBOOL;
        break;
      case ControlType.BYTE:
        title = buildBYTETitle();
        content = buildBYTEContent();
        onOkButtonPressed = onOkButtonPressedBYTE;
        break;
      case ControlType.IEC958:
        title = buildIEC958Title();
        content = buildIEC958Content();
        onOkButtonPressed = onOkButtonPressedIEC958;
        break;
      case ControlType.INT64:
        title = buildINT64Title();
        content = buildINT64Content();
        onOkButtonPressed = onOkButtonPressedINT64;
        break;
      case ControlType.Unknown:
        title = buildUnknownTitle();
        content = buildUnknownContent();
        onOkButtonPressed = onOkButtonPressedUnknown;
        break;
      default:
        throw Exception();
    }

    var alertDialogActions = <Widget>[
      StatefulBuilder(
        key: okButtonKey,
        builder: (context, setState) {
          return FlatButton(
            child: Text(AppLocalizations.of(context).okCapital),
            onPressed: okButtonEnabled ? onOkButtonPressed : null,
          );
        },
      ),
    ];

    if (widget.control.type != ControlType.Unknown) {
      alertDialogActions.insert(
        0,
        FlatButton(
          child: Text(AppLocalizations.of(context).cancelCapital),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      );
    }

    return AlertDialog(
      title: title,
      content: content,
      actions: alertDialogActions,
    );
  }

  Widget buildINTTitle() {
    final dsrangeStartIndex = widget.details.indexOf('(') + 1;
    final dsrangeEndIndex = widget.details.length - 1;
    final dsrange =
        widget.details.substring(dsrangeStartIndex, dsrangeEndIndex);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(widget.control.name),
        Text(
          dsrange,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 16,
          ),
        ),
      ],
    );
  }

  Widget buildENUMTitle() {
    return Text(widget.control.name);
  }

  Widget buildBOOLTitle() {
    return Text(widget.control.name);
  }

  Widget buildBYTETitle() {
    return Text(widget.control.name);
  }

  Widget buildIEC958Title() {
    return Text(widget.control.name);
  }

  Widget buildINT64Title() {
    return Text(widget.control.name);
  }

  Widget buildUnknownTitle() {
    return Text(widget.control.name);
  }

  Widget buildINTContent() {
    textControllersINT = widget.control.value.map(
      (intValue) {
        final controller = TextEditingController(text: intValue.toString());
        controller.selection = TextSelection.fromPosition(
          TextPosition(offset: controller.text.length),
        );
        return controller;
      },
    ).toList();

    return Scrollbar(
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return TextField(
            autofocus: index == 0,
            keyboardType: TextInputType.numberWithOptions(signed: true),
            controller: textControllersINT[index],
            onChanged: (text) {
              okButtonKey.currentState.setState(() {
                okButtonEnabled = textControllersINT.every((controller) {
                  return controller.text.isNotEmpty;
                });
              });
            },
          );
        },
        itemCount: widget.control.num,
      ),
    );
  }

  Widget buildENUMContent() {
    final detailsWithoutName = widget.details.split(':').last.trim();
    final possibleValues = detailsWithoutName.split(' ').map((possibleValue) {
      return possibleValue.replaceFirst('>', '');
    }).toList();

    selectedENUM ??=
        widget.control.value.map((value) => value as String).toList();

    final valueLength = widget.control.value.length;
    final partValueLength = (possibleValues.length / valueLength).truncate();

    return Scrollbar(
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          final partIndex = (index / partValueLength).truncate();

          final radioListTile = RadioListTile<String>(
            title: Text(possibleValues[index]),
            value: possibleValues[index],
            groupValue: selectedENUM[partIndex],
            onChanged: (value) {
              setState(() {
                selectedENUM[partIndex] = value;
              });
            },
          );

          if ((index + 1) % partValueLength == 0 &&
              partIndex < valueLength - 1) {
            return Column(
              children: <Widget>[
                radioListTile,
                Divider(),
              ],
            );
          }

          return radioListTile;
        },
        itemCount: possibleValues.length,
      ),
    );
  }

  Widget buildBOOLContent() {
    switchBOOL ??= widget.control.value.first;
    final valueAsString = switchBOOL ? 'On' : 'Off';

    return ListTile(
      title: Text(valueAsString),
      trailing: Switch(
        onChanged: (newValue) {
          setState(() {
            switchBOOL = newValue;
          });
        },
        value: switchBOOL,
      ),
    );
  }

  Widget buildBYTEContent() {
    return Text('BYTE is currently unsuported');
  }

  Widget buildIEC958Content() {
    return Text('IEC958 is currently unsuported');
  }

  Widget buildINT64Content() {
    return Text('INT64 is currently unsuported');
  }

  Widget buildUnknownContent() {
    return Text('Unknown');
  }

  void onOkButtonPressedINT() {
    final newValue = textControllersINT.map((controller) {
      return controller.text.trim();
    }).join(' ');

    Navigator.pop(context, newValue);
  }

  void onOkButtonPressedENUM() {
    final newValue = selectedENUM.join(' ');

    Navigator.pop(context, newValue);
  }

  void onOkButtonPressedBOOL() {
    Navigator.pop(context, switchBOOL ? '1' : '0');
  }

  void onOkButtonPressedBYTE() {
    Navigator.pop(context);
  }

  void onOkButtonPressedIEC958() {
    Navigator.pop(context);
  }

  void onOkButtonPressedINT64() {
    Navigator.pop(context);
  }

  void onOkButtonPressedUnknown() {
    Navigator.pop(context);
  }
}

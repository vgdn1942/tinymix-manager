import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';

class DraggableScrollbarRrect extends StatelessWidget {
  final BoxScrollView child;
  final ScrollController controller;
  final double itemExtent;
  final bool showLabel;

  const DraggableScrollbarRrect({
    Key key,
    @required this.controller,
    @required this.itemExtent,
    @required this.child,
    this.showLabel = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollbar.rrect(
      controller: controller,
      backgroundColor: Theme.of(context).primaryColor,
      labelTextBuilder: showLabel
          ? (offset) {
              return Text(
                '${offset ~/ itemExtent}',
                style: TextStyle(color: Colors.white),
              );
            }
          : null,
      child: child,
    );
  }
}
